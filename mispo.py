import pymisp as pm, warnings
import os
import requests
import json
import datetime
from datetime import timedelta
import sys
from pymisp import MISPEvent,MISPObject

misp = pm.ExpandedPyMISP(url="https://192.168.178.129",
                 key="w0IjImoGQ4Ve9SZLohFOphDehXYvcJSw9yhDkFaN",
                 ssl=False)
                 
warnings.filterwarnings("ignore")

url = 'https://www.virustotal.com/vtapi/v2/file/search'

report_url = 'https://www.virustotal.com/vtapi/v2/file/report'

cur_date = datetime.datetime.now()-timedelta(days=1)
yesterday = datetime.datetime.strftime(cur_date,"%Y-%m-%d")

families =  ['emotet','trickbot','qakbot','zloader','bazarloader','icedid']
for family in families:
##################### Create MISP event##########################################
    event = MISPEvent()
    event.info =   str(family)+"_IOCs"  # Required
    event.distribution = 0  # Optional, defaults to MISP.default_event_distribution in MISP config
    event.threat_level_id = 2  # Optional, defaults to MISP.default_event_threat_level in MISP config
    event.analysis = 1  # Optional, defaults to 0 (initial analysis)            
    #misp.add_tag(str(family) + ' ' +"IOCs")
    misp.add_tag({"name":family})
################################################################################## 
    i =0
    query_string = "microsoft:" + family + ' ' + "fs:" + yesterday + "+" +' '+ "p:20- and p:5+"
    params = {'apikey': 'e06e26fdccc746b92a756cc3fc53ac4668aefbab8bb6fdddf6826ea0d3bff7d5', 'query': 'microsoft:emotet fs:2022-01-19+'}
    response = requests.get(url, params=params)
    cont = response.json()    
    if cont['response_code'] == 1 and len(cont['hashes']) > 2:
        for hash in cont['hashes']:                                     
            misp_object1 = event.add_object(name='intelmq_event', comment='file Attributes')
            misp_object2 = event.add_object(name='cytomic-orion-file', comment='file Attributes')
            
            params = {'apikey': 'e06e26fdccc746b92a756cc3fc53ac4668aefbab8bb6fdddf6826ea0d3bff7d5', 'resource': hash, 'allinfo':'true'}
            res = requests.get(report_url, params=params)            
            cont1 = res.json()                                
                                 
            if cont1['first_seen'] != [] and cont1['first_seen'] != None:
                misp_object1.add_attribute ('time.source', value= cont1['first_seen'])                            

            if cont1['tags'] != [] and cont1['tags'] != None:
                misp_object1.add_attribute ("comment", value= cont1['tags'])
                
            if cont1['md5'] != [] and cont1['md5'] != None:
                misp_object1.add_attribute ("malware.hash.md5", value= cont1['md5'])                                 
                
            if cont1['sha256'] != [] and cont1['sha256'] != None:
                misp_object1.add_attribute ("malware.hash.sha256", value= cont1['sha256'])                                 
                
            if cont1['sha1'] != [] and cont1['sha1'] != None:
                misp_object1.add_attribute ("malware.hash.sha1", value= cont1['sha1'])                                 
                                            
            if cont1['ITW_urls'] != [] and cont1['ITW_urls'] != None:
                misp_object1.add_attribute ("feed.url", value= cont1['ITW_urls'])
                
            misp_object1.add_attribute("malware.name", value= family)
                            
            if cont1['first_seen'] != [] and cont1['first_seen'] != None:
                misp_object2.add_attribute ('first-seen', value= cont1['first_seen'])                            
                
            if cont1['last_seen'] != [] and cont1['last_seen'] != None:
                misp_object2.add_attribute ('last-seen', value= cont1['last_seen'])                            
            
            if cont1['size'] != [] and cont1['size'] != None:
                misp_object2.add_attribute ("fileSize", value= cont1['size'])
            
            if i ==0:
                misp.add_event(event)
            
            i = i+1
            misp.update_event(event)
            #print(event.to_json())